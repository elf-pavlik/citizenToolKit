<?php

/*
badges collection : badges
    "tag" : "crowdfunder",
    "label" : "Crowdfunder",
    "icon" : [ 
        "fa-euro", 
        "fa-bookmark"
    ],
    "img" : "",
    color : ""
[x] add badges based on tags 
[x] add a new badge : connect a badge to a tag 
[ ] edit
	carefull about moderation and loosing what someone else cose 
	edting with consensus or versions
[x] count the number of people carrying a tag or badge 
[x] add color to dynform 
[ ] color picker
[x] add image type badges
[ ] badges == une facon d'unifier les tags 
[ ] clean up dans les tags
[ ] listing de tout les icone font awesome
[ ] communities and expert networks based on badges
[ ] page competence : in this community we do this (list of badges)
	see community by badges
[ ] how to connect badges and roles 
	maybe prefill roles with badges when inviting someone 
[ ] comment faire des badge ambiance dans un contexte
	ex : coecrire l'emotion autour d'un event 
	ce serait avec une interface real time
[ ] test duplicate badges based on duplicate tags
[ ] add this badge btn > simply adds the corresponding tag to the person
	on each badge btn "I am" > adds the tag to the persons profile
	[ ] une fois ajouter show all people with a badge , btn in my scope
	[ ] btn : show activity of a badge (of a tag behind a badge )
[ ] survey : who are you based on tags and badges
[ ] many tag badge : badges that only show when a all given tags exist
[ ] moderation ou notification de la création d'un neau badge
[ ] in index page if click a badge preview list of people, or activity of that tag

*/
class Badge {
	
	const COLLECTION = "badges"; 

	public static $dataBinding = array (
        
        "name" => array("name" => "name", "rules" => array("required")),
        "icon" => array("name" => "icon"),
        "color" => array("name" => "color"),
        "tags" => array("name" => "tags", "rules" => array("required")),
        "img" => array("name" => "img"),
        "parent" => array("name" => "parent"),
        "description" => array("name" => "description"),
        "synergie" => array("name" => "synergie"),
        "structags" => array("name" => "structags"),
        "source" => array("name" => "source"),
        "category" => array("name" => "category"),
        
        "modified" => array("name" => "modified"),
        "updated" => array("name" => "updated"),
        "creator" => array("name" => "creator"),
        "created" => array("name" => "created")
    );

	public static function getBagdes($idItem, $typeItem) {
		$badges = array();
		$account = PHDB::findOneById($typeItem ,$idItem, array("badges"));
		if(!empty($account["badges"]))
			$badges = $account["badges"];
		return $badges;
	}
	public static function getByWhere($where, $img=false) {
		//$badges = array();
		return PHDB::find(self::COLLECTION,$where);
		/*if(!empty($account["badges"]))
			$badges = $account["badges"];
		return $badges;*/
	}
	public static function getOneByWhere($where, $img=false) {
		//$badges = array();
		return PHDB::findOne(self::COLLECTION,$where);
		/*if(!empty($account["badges"]))
			$badges = $account["badges"];
		return $badges;*/
	}
	public static function getBadgesByLinks($links) {
		$badges = array();
		foreach ($links as $sec => $l) {
			foreach ($l as $id => $v) {
				if(@$v["type"]){
					$el = Element::getByTypeAndId( $v["type"],$id );
					if( @$el["preferences"]["badge"] ){
						$b = array( "name"=>$el["name"],
								    "type"=>$v["type"]);
						if(!empty($el["profilThumbImageUrl"]))
							$b["img"] = $el["profilThumbImageUrl"];
						$badges[] = $b;
					}
				}
			}
		}
		
		return $badges;
	}

	public static function checkBadgeInListBadges($badge, $badges) {
		
		$res = false ;
		/*foreach ($badges as $key => $value) {
			if($badge == $value["name"]){
				$res = true ;
				break;
			}
		}*/
		return (@$badges[$badge]) ? true : false;
	}

	public static function addBadgeInListBadges($badge, $badges) {
		$res = array(	"result" => false, 
						"badges" => $badges, 
						"msg" => Yii::t("import","Le badge est déjà dans la liste"));
		if(is_array($badge)){
			$newListBadges = array();
			foreach ($badge as $key => $value) {
				if(!self::checkBadgeInListBadges((empty($value["name"])?$value:$value["name"]), $badges)){
					$newBadge["name"] = (empty($value["name"])?$value:$value["name"]);
					$newBadge["date"] = (empty($value["date"])?new mongoDate(time()):$value["date"]);
					$newListBadges[] = $newBadge;
				}
			}
			$badges = array_merge($badges, $newListBadges);
			$res = array("result" => true, "badges" => $badges);

		}else if(is_string($badge)){
			if(!self::checkBadgeInListBadges($badge, $badges)){
				$newBadge["name"] = $badge;
				$newBadge["date"] = new mongoDate(time());
				$badges[] = $newBadge;
				$res = array("result" => true, "badges" => $badges);
			}
		}
		return $res;
	}

	public static function updateBadges($badges, $idItem, $typeItem) {
		$res = array("result" => false, "msg" => Yii::t("import","La mise à jour a échoué."));
		if($typeItem == Person::COLLECTION)
			$res = Person::updatePersonField($idItem, "badges", $badges, Yii::app()->session["userId"]);
		else if($typeItem == Organization::COLLECTION)
			$res = Organization::updateOrganizationField($idItem, "badges", $badges, Yii::app()->session["userId"]);
		else if($typeItem == Event::COLLECTION)
			$res = Event::updateEventField($idItem, "badges", $badges, Yii::app()->session["userId"]);
		else if($typeItem == Project::COLLECTION)
			$res = Project::updateProjectField($idItem, "badges", $badges, Yii::app()->session["userId"]);

		return $res;
	}

	public static function addAndUpdateBadges($nameBadge, $idItem, $typeItem) {
		$badges = self::getBagdes($idItem, $typeItem);
		if(empty($badges))
           $badges = array();

       	$resAddBadge = self::addBadgeInListBadges($nameBadge, $badges);
		if($resAddBadge["result"] == true){
			$res = self::updateBadges($resAddBadge["badges"], $idItem, $typeItem);
		}else
			$res = array("result" => false, "msg" => $resAddBadge["msg"]);

		return $res;
	}


	public static function conformeBadges($badges) {
		$newListBadges = array();
		if(is_array($badges)){
			foreach ($badges as $key => $value) {
				$newBadge["name"] = (empty($value["name"])?$value:$value["name"]);
				$newBadge["date"] = (empty($value["date"])?new mongoDate(time()):$value["date"]);
				$newListBadges[] = $newBadge;
			}
		}else if(is_string($badges)){
			$newBadge["name"] = $badges;
			$newBadge["date"] = new mongoDate(time());
			$newListBadges[] = $newBadge;
		}
		return $newListBadges;
	}


	public static function delete($nameBadge, $idItem, $typeItem) {
		$badges = self::getBagdes($idItem, $typeItem);
		$newBadges = array();
		foreach ($badges as $key => $badge) {
			if($badge["name"] != $nameBadge)
				$newBadges[] = $badge;
		}
		$res = self::updateBadges($newBadges, $idItem, $typeItem);
		return $res ;
	}

}

?>