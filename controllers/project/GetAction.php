<?php
class GetAction extends CAction {

	public function run($id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null, $category = null) {
		$controller=$this->getController();
		
		// Get format
		if( !empty(Yii::app()->session['costum'] )){
			$test = ucfirst(Yii::app()->session['costum']["slug"]);
			if(!empty($test::$dataBinding_allProject))
				$bindMap = $test::$dataBinding_allProject;
		}
		
		if( empty($bindMap) &&  $format == Translate::FORMAT_SCHEMA)
			$bindMap = (empty($id) ? TranslateSchema::$dataBinding_allProject : TranslateSchema::$dataBinding_project);
		else if ( empty($bindMap) && $format == Translate::FORMAT_KML) 
			$bindMap = (empty($id) ? TranslateKml::$dataBinding_allProject : TranslateKml::$dataBinding_project);
		else if (empty($bindMap) && $format == Translate::FORMAT_GEOJSON)
			$bindMap = (empty($id) ? TranslateGeoJson::$dataBinding_allProject : TranslateGeoJson::$dataBinding_project);
		else if (empty($bindMap) && $format == Translate::FORMAT_JSONFEED)
			$bindMap = TranslateJsonFeed::$dataBinding_allProject;
		else if( empty($bindMap) && $format == Translate::FORMAT_MD || $format == Translate::FORMAT_TREE)
			$bindMap = Project::CONTROLLER;
		else if( empty($bindMap) )
			$bindMap = (empty($id) ? TranslateCommunecter::$dataBinding_allProject : TranslateCommunecter::$dataBinding_project);
		$p = array();
		if(!empty($category))
			$p["category"] = $category;
		//Rest::json($p); exit;
		$result = Api::getData($bindMap, $format, Project::COLLECTION, $id,$limit, $index, $tags, $multiTags, $key, $insee, null, null, null, null, $p);

		

		if ($format == Translate::FORMAT_KML) {
			$strucKml = News::getStrucKml();    
			Rest::xml($result, $strucKml,$format);
		}else if ($format == "csv") {
			//Rest::csv($result["entities"]);
			Rest::csv($result["entities"], false, true, Project::COLLECTION);
		}else    
			Rest::json($result);

		Yii::app()->end();
	}
}