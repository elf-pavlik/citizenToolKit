<?php
class GetMailingListAction extends CAction
{
    public function run($type = null)
    {
        $controller = $this->getController();
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

            if(empty($type)){
				$notMail = PHDB::find(ActivityStream::COLLECTION, array("verb" => ActStr::VERB_NOSENDING));
				$notM = array();
				foreach ($notMail as $key => $value) {
					$notM[] = $value["target"]["email"];
				}
				//return Rest::json($notM);

				$where = array("preferences.sendMail" => true, "email" => array('$nin' => $notM ));
				$email = PHDB::find(Person::COLLECTION, $where, array("email"));
				Rest::csv($email);
			} else if ($type == "cofinanceur") {
				$notMail = PHDB::find(ActivityStream::COLLECTION, array("verb" => ActStr::VERB_NOSENDING));
				$notM = array();
				foreach ($notMail as $key => $value) {
					$notM[] = $value["target"]["email"];
				}

				$orga = Slug::getElementBySlug("cofinanceur", array("name") );
				$where = array(	"preferences.sendMail" => true, 
								"email" => array('$nin' => $notM ),
								"links.memberOf.".$orga["id"] => array('$exists' => true) );
				$email = PHDB::find(Person::COLLECTION, $where, array("email"));

				Rest::csv($email);
			} else if ($type == "other") {
				$notMail = PHDB::find(ActivityStream::COLLECTION, array("verb" => ActStr::VERB_NOSENDING));
				$notM = array();
				foreach ($notMail as $key => $value) {
					$notM[] = $value["target"]["email"];
				}

				$orga = Slug::getElementBySlug("cofinanceur", array("name") );
				$where = array(	"preferences.sendMail" => true, 
								"email" => array('$nin' => $notM ),
								"links.memberOf.".$orga["id"] => array('$exists' => false));
				$email = PHDB::find(Person::COLLECTION, $where, array("email"));

				Rest::csv($email);
			}
		} else {
			Rest::json(array("result" => false, 
								"msg" => Yii::t("common","You are not admin") )); 
		}
	}
}

?>