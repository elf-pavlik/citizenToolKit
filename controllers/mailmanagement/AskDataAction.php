<?php

class AskDataAction extends CAction {
    
	public function run() {
		//$controller = $this->getController();
		try {
			$res = array("result" => false);
			$params = $_POST;
			$valid = DataValidator::email( $params["email"] ) ;
			if( $valid  != "")
				throw new CTKException(Yii::t("common","The email is not well formated"));
			else{
				if(Authorisation::verifCaptcha($params['captchaHash'], $params['captchaUserVal']) ){
					$params["getData"] = true ;
					$params["date"] = time() ;
					unset($params["captchaUserVal"]);
					unset($params["captchaHash"]);
					PHDB::insert(Cron::ASK_COLLECTION, $params);
					$id = (String) $params["_id"] ;
					unset($params["_id"]);





					$info = Element::getDataByAsk($id);
					$resume = array();
					if(!empty($info)){
						if(!empty($info["person"])){
							$resume["account"] = true ;
							if(!empty($info["person"]["roles"]) && !empty($info["person"]["roles"]["tobeactivated"]) && $info["person"]["roles"]["tobeactivated"] ){
								$resume["tobeactivated"] = true ;
							}
							if(!empty($info["person"]["invitedBy"]) ){
								$resume["invitedBy"] = true ;
							}

							
						}

						if(!empty($info["elts"]) ){
							foreach ($info["elts"] as $kElt => $valElt) {
								$resume["elts"][$kElt] = count($valElt) ;
							}
						}
					}
					$params["resume"] = $resume;
					$params["attach"] = $id;
					$mail = array (
		                "type" => Cron::TYPE_MAIL,
		                "tpl"=>"askdata",
		                "subject" => "[ Communecter ]".Yii::t("common","Your data"),
		                "from"=>Yii::app()->params['adminEmail'],
		                "to" => $params["email"],
		                "attach" => $id,
		                "tplParams" => Mail::initTplParams($params),
		            );
		            $mail=Mail::getCustomMail($mail);
		            Mail::schedule($mail);

		            $res = array("result" => true);
		        } else {
		        	$res["msg"] = Yii::t("common","Incorrect security code");
		        }
			}
			
		} catch (Exception $e) {
			$res["msg"] = $e->getMessage();
		}
		return Rest::json($res);
	}
}