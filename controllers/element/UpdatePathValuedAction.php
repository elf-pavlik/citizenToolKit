<?php
/**
* Update an information field for a person
*/
class UpdatePathValuedAction extends CAction
{
    public function run()
    {
        $controller=$this->getController();
        if (!empty($_POST["collection"]) && !empty($_POST["id"]) && @$_POST["path"] ) {
			try {
				//updatePathValue($collection, $id, $path, $value, $arrayForm=null, $aedit=null, $pull=null) {
                if($_POST["value"] === "true" || $_POST["value"] === "false" )
                    $_POST["value"] = ($_POST["value"] === 'true') ? true: false;
				return Element::updatePathValue($_POST["collection"],$_POST["id"], $_POST["path"],$_POST["value"] , @$_POST["arrayForm"], @$_POST["edit"], @$_POST["pull"]);
			} catch (CTKException $e) {
				return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), $_POST["path"]=>$_POST["value"]));
			}
		} else {
          return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Invalid request")));
        }
        return Rest::json(array("result"=>true, "msg"=>Yii::t("common","Information updated"),"text"=> $_POST["value"],"id"=>$_POST["id"]));
    }
}