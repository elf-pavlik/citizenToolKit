<?php

class GetAction extends CAction {
/**
* Get element by Id and type
*/

  public function run($type,$id, $update=null, $edit=null) { 
    //if (isset(Yii::app()->session["userId"])) {
      try {
        $res = array("result" => true, "map" => Element::getByTypeAndId($type,$id, null, $update) );
        //pour :es type POI 
        //ex used in doc module
        if( $type == Poi::COLLECTION && $edit){
          $documents  = PHDB::find(Document::COLLECTION, array('id' => $id, "type" => $type ));
          if( count($documents) )
          {
            $res["map"]["documents"] = array();
            foreach ($documents as $key => $doc) {
              $res["map"]["documents"][] = array (
                "name" => $doc['name'], 
                "doctype" => $doc['doctype'],  
                "path" => Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$doc["moduleId"]."/".$doc["folder"]."/".$doc['name']
              );
            }
          }

          $res["map"]["editBtn"] = $this->getController()->renderPartial("costum.views.tpls.openFormBtn",
                                array(
                                    'edit' => "update",
                                    'tag' => null,
                                    'id' => $res["map"]["_id"] ),true);
        }
      } catch (CTKException $e) {
        $res = array("result"=>false, "msg"=>$e->getMessage());
      }
    // } else {
    //   $res = array("result"=>false, "msg"=>"Please login first");
    // }
    Rest::json($res);
  }

}

?>