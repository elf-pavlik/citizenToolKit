<?php
/**
* retreive dynamically 
*/
class SearchAction extends CAction
{
    public function run($page="search", $type=null) {
    	$controller=$this->getController();        
        CO2Stat::incNbLoad("co2-search");   

        $params = array("type" => "all", "page"=>$page);
        echo $controller->renderPartial("search", $params, true);
    }
}