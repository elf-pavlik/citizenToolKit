<?php

class HomeAction extends CAction {
/**
* Dashboard Organization
*/
    public function run() { 
    	$controller=$this->getController();
        $this->getController()->layout = "//layouts/mainSearch";
		CO2Stat::incNbLoad("co2-home");
    	if( !isset(Yii::app()->session["userId"]))
        	$this->redirect( Yii::app()->createUrl($controller->module->id) );
    	if(Yii::app()->request->isAjaxRequest)
        	echo $controller->renderPartial("home", array(), true);
   		else 
			$controller->render( "home" , array());
    }
}