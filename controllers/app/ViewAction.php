<?php

class ViewAction extends CAction {
/**
* Dashboard Organization
*/
    public function run($page=null,$url=null, $extra=null){
        $controller=$this->getController();
        $this->getController()->layout = "//layouts/mainSearch";
        CO2Stat::incNbLoad("co2-info");  
        $params=array("page"=>@$page);
        if(!empty($extra)){
        	$extraP=explode(".", $extra);
        	$i=0;
        	$length=count($extraP);
        	for ($i=0; $i < $extraP;) { 
        		$params[$extraP[$i]]=$extraP[$i+1];
        		$i+=2;
        	}
        }
        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($url, $params, true);
        else 
            $controller->render( $url , $params);
    }
}